package com.idlemindsworkshop.hitori;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

public class Game extends Activity {
	private static final String TAG = "Hitori";
	private static final String PREF_PUZZLE = "puzzle";
	public static final String KEY_SIZE = "com.idlemindsworkshop.hitori.size";
	protected static final int SIZE_CONTINUE = -1;
	public static final int SIZE_SMALL = 0;
	public static final int SIZE_MEDIUM = 1;
	public static final int SIZE_HUGE = 2;

	private int puzzle[];
	protected int puzzsize;

	private PuzzleView puzzleView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate");

		int size = getIntent().getIntExtra(KEY_SIZE, SIZE_SMALL);
		used = new int[puzzsize][puzzsize][];
		puzzle = new int[puzzsize * puzzsize];
		puzzle = getPuzzle(size);
//		calculateUsedTiles();			removed because of out of bounds issue, needs to be reinstated(?)

		puzzleView = new PuzzleView(this);
		setContentView(puzzleView);
		puzzleView.requestFocus();
		
		// If activity is restarted, do a continue next time
		getIntent().putExtra(KEY_SIZE, SIZE_CONTINUE);
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "onPause");
		// Music.stop(this);

		// Save current puzzle
		getPreferences(MODE_PRIVATE).edit()
				.putString(PREF_PUZZLE, toPuzzleString(puzzle)).commit();
	}

	protected boolean setTitleIfValid(int x, int y, int value) {
		int tiles[] = getUsedTiles(x, y);
		if (value != 0) {
			for (int tile : tiles) {
				if (tile == value)
					return false;
			}
		}
		setTile(x, y, value);
		calculateUsedTiles();
		return true;
	}

	private int used[][][];

	protected int[] getUsedTiles(int x, int y) {
		return used[x][y];
	}

	private void calculateUsedTiles() {
		for (int x = 0; x < puzzsize; x++) {
			for (int y = 0; y < puzzsize; y++) {
				used[x][y] = calculateUsedTiles(x, y);
				// Log.d(TAG, "used[" + x + "][" + y + "] = " +
				// toPuzzleString(used[x][y]));
			}
		}
	}

	private int[] calculateUsedTiles(int x, int y) {
		int c[] = new int[puzzsize];
		// horizontal
		for (int i = 0; i < puzzsize; i++) {
			if (i == x)
				continue;
			int t = getTile(i, y);
			if (t != 0)
				c[t - 1] = t;
		}

		// vertical
		for (int i = 0; i < puzzsize; i++) {
			if (i == y)
				continue;
			int t = getTile(x, i);
			if (t != 0)
				c[t - 1] = t;
		}
		// same cell block
		/// really need to modify
		int startx = (x / 3) * 3;
		int starty = (y / 3) * 3;
		for (int i = startx; i < startx + 3; i++) {
			for (int j = starty; j < starty + 3; j++) {
				if (i == x && j == y)
					continue;
				int t = getTile(i, j);
				if (t != 0)
					c[t - 1] = t;
			}
		}
		// compress
		int nused = 0;
		for (int t : c) {
			if (t != 0)
				nused++;
		}
		int c1[] = new int[nused];
		nused = 0;
		for (int t : c) {
			if (t != 0)
				c1[nused++] = t;
		}
		return c1;
	}

	private final int smallSize = 5;
	private final int mediumSize = 8;
	private final int hugeSize = 10;
	
	private final String smallPuzzle = "2234251214244155541343521";
	private final String mediumPuzzle = "650000070000506000014000005"
			+ "007009000002314700000700800" + "500000630000201000030000097";
	private final String hugePuzzle = "2234251214244155541343521" + "2234251214244155541343521" +
			"2234251214244155541343521" + "2234251214244155541343521";

	private int[] getPuzzle(int size) {
		String puzz = null;
		// TODO: Continue previous game
		switch (size) {
		case SIZE_CONTINUE:
			puzz = getPreferences(MODE_PRIVATE).getString(PREF_PUZZLE,
					smallPuzzle);
			break;
		case SIZE_HUGE:
			puzzsize = hugeSize;
			puzz = hugePuzzle;
			break;
		case SIZE_MEDIUM:
			puzzsize = mediumSize;
			puzz = mediumPuzzle;
			break;
		case SIZE_SMALL:
			puzzsize = smallSize;
			puzz = smallPuzzle;
			break;
		}
		return fromPuzzleString(puzz);
	}

	static private String toPuzzleString(int[] puz) {
		StringBuilder buf = new StringBuilder();
		for (int element : puz) {
			buf.append(element);
		}
		return buf.toString();
	}

	static protected int[] fromPuzzleString(String string) {
		int[] puz = new int[string.length()];
		for (int i = 0; i < puz.length; i++) {
			puz[i] = string.charAt(i) - '0';
		}
		return puz;
	}

	private int getTile(int x, int y) {
		return puzzle[y * puzzsize + x];
	}

	private void setTile(int x, int y, int value) {
		puzzle[y * puzzsize + x] = value;
	}

	protected String getTileString(int x, int y) {
		int v = getTile(x, y);
		if (v == 0)
			return "";
		else
			return String.valueOf(v);
	}

	// really need to change
	protected boolean setTileIfValid(int x, int y, int value) {
		int tiles[] = getUsedTiles(x, y);
		if (value != 0) {
			for (int tile : tiles) {
				if (tile == value)
					return false;
			}
		}
		setTile(x, y, value);
		calculateUsedTiles();
		return true;
	}

}
