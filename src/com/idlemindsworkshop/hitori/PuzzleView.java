package com.idlemindsworkshop.hitori;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;


public class PuzzleView extends View{
	private static final String TAG = "Hitori";
	private final Game game;
	public PuzzleView(Context context) {
		super(context);
		this.game = (Game) context;
		setFocusable(true);
		setFocusableInTouchMode(true);
	}
	
	private float width;
	private float height;
	private int selX;
	private int selY;
	private final Rect selRect = new Rect();		// selection cursor
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		width = w / (float) game.puzzsize;
		height = h / (float) game.puzzsize;
		getRect(selX, selY, selRect);
		Log.d(TAG, "onSizeChanged: width "+ width + ", height " + height);
		super.onSizeChanged(w, h, oldw, oldh);
	}

	private void getRect(int x, int y, Rect rect) {
		rect.set((int)(x * width), (int)(y * height), 
				(int)(x * width + width), (int)( y * height + height));
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		// Draw background
		Paint background = new Paint();
		background.setColor(getResources().getColor(R.color.puzzle_background));
		canvas.drawRect(0, 0, getWidth(), getHeight(), background);
		
		// Draw board
		// Define colors for grid lines
		Paint dark = new Paint();
		dark.setColor(getResources().getColor(R.color.puzzle_dark));
		
		Paint hilite = new Paint();
		hilite.setColor(getResources().getColor(R.color.puzzle_hilite));
		
		Paint light = new Paint();
		light.setColor(getResources().getColor(R.color.puzzle_light));
		
		// Draw minor grid lines
		for (int i = 0; i < game.puzzsize; i++) {
			if (i % 3 != 0)
				continue;
			canvas.drawLine(0, i * height, getWidth(), i * height, light);
			canvas.drawLine(0, i * height + 1, getWidth(), i * height + 1, hilite);
			canvas.drawLine(i * width, 0, i * width, getHeight(), light);
			canvas.drawLine(i * width + 1, 0, i * width + 1, getHeight(), hilite);
		}
		
		// Draw major grid lines
		for (int i = 0; i < game.puzzsize; i++) {
			if (i % 3 != 0)
				continue;
			canvas.drawLine(0, i * height, getWidth(), i * height, dark);
			canvas.drawLine(0, i * height + 1, getWidth(), i * height + 1, hilite);
			canvas.drawLine(i * width, 0, i * width, getHeight(), dark);
			canvas.drawLine(i * width + 1, 0, i * width + 1, getHeight(), hilite);
		}
		
		// Draw the numbers, Mason!
		// Define color and style for numbers
		Paint foreground = new Paint(Paint.ANTI_ALIAS_FLAG);
		foreground.setColor(getResources().getColor(R.color.puzzle_foreground));
		foreground.setStyle(Style.FILL);
		foreground.setTextSize(height * 0.75f);			// font height is 3/4ths tile height
		foreground.setTextScaleX(width / height);
		foreground.setTextAlign(Paint.Align.CENTER);
		
		// Draw the number in the center of the tile
		FontMetrics fm = foreground.getFontMetrics();
		// Centering in X: use alignment (and X at midpoint)
		float x = width / 2;
		// Center in Y: measure ascent/descent first
		float y = height / 2 - (fm.ascent + fm.descent) / 2;
		for (int i = 0; i < game.puzzsize; i++) {
			for (int j = 0; j < game.puzzsize; j++) {
				canvas.drawText(this.game.getTileString(i, j), 
						i * width + x, j * height + y, foreground);
			}
		}
		
		// Draw the selection
		Log.d(TAG, "selRect=" + selRect);
		Paint selected = new Paint();
		selected.setColor(getResources().getColor(R.color.puzzle_selected));
		canvas.drawRect(selRect, selected);

		/*
		   //Draw the hints ///which may not be implemented
		   // Pick a hint color based on ///stuff internal to algorithm
		   Paint hint = new Paint();
		   int c[] = { getResources().getColor(R.color.puzzle_hint_0), 
				   getResources().getColor(R.color.puzzle_hint_1), getResources().getColor(R.color.puzzle_hint_2), };
		   Rect r = new Rect();
		   for (int i = 0; i < game.puzzsize; i++) {
			   for (int j = 0; j < game.puzzsize; j++) {
				   int movesleft = game.puzzsize - game.getUsedTiles(i, j).length;
				   if (movesleft < c.length) {				// if there's only 1 or 2 moves left for that square
					   getRect(i, j, r);
					   hint.setColor(c[movesleft]);
					   canvas.drawRect(r, hint);
				   }
			   }
		   }
		*/   
	}
	
   @Override
   public boolean onKeyDown(int keyCode, KeyEvent event) {
       Log.d(TAG, "onKeyDown: keycode=" + keyCode + ", event=" + event);
   switch(keyCode) {
   case KeyEvent.KEYCODE_DPAD_UP:
       select(selX, selY - 1);
       break;
   case KeyEvent.KEYCODE_DPAD_DOWN:
       select(selX, selY + 1);
       break;
   case KeyEvent.KEYCODE_DPAD_LEFT:
       select(selX - 1, selY);
       break;
   case KeyEvent.KEYCODE_DPAD_RIGHT:
       select(selX + 1, selY);
       break;
   default:
	   return super.onKeyDown(keyCode, event);
   		}
   return true;
   }

   /* Note: only draw in onDraw(). 
    * invalidate() marks rectangles as dirty. 
    * The window manager will combine dirty rectangles (the clip region, only area on screen 
    	 that changes) and call onDraw() again.	*/
   private void select(int x, int y) {
	   invalidate(selRect);						// area covered by old selection rect has to be redrawn to non-cursor color
	   selX = Math.min(Math.max(x, 0), 8);
	   selY = Math.min(Math.max(y, 0), 8);
	   getRect(selX, selY, selRect);
	   invalidate(selRect);						// new selection area needs to be redrawn to cursor color
   }
   
   @Override
   public boolean onTouchEvent(MotionEvent event) {
	   if(event.getAction() != MotionEvent.ACTION_DOWN)
		   return super.onTouchEvent(event);
	   
	   select((int)(event.getX()/width), 
			   (int)(event.getY()/height));
	   // possibly do something here
	   Log.d(TAG, "onTouchEvent: x " + selX + ", y " + selY);
	   return true;
   }
   
   // need to tie the following to changing tile status (vs. number), see on 78-79
   public void setSelectedTile(int tile) {
	   if (game.setTileIfValid(selX, selY, tile)) {
		   invalidate();			// may change hints ///need to change to invalidate only selected tile
	   } else {
		   // selection is somehow not valid
		   Log.d(TAG, "setSelectedTile: invalid: " + tile);
		   startAnimation(AnimationUtils.loadAnimation(game, R.anim.shake));
	   }
   }   
   
}
